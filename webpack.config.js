const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const resolvePath = relativePath => path.resolve(__dirname, relativePath);

module.exports = {
  entry: {
    background: resolvePath("src/background.ts"),
    options: resolvePath("src/options.tsx"),
    "site/twitter/content": resolvePath("src/site/twitter/content.ts"),
    "site/pixiv/content": resolvePath("src/site/pixiv/content.ts"),
    "site/pixiv/page": resolvePath("src/site/pixiv/page.ts")
  },
  mode: "production",
  devtool: "source-map",
  module: {
    noParse: [/moment.js/], // https://github.com/ant-design/ant-design/issues/6338
    rules: [
      {
        test: /\.(tsx?|jsx?)$/u,
        include: /src/u,
        exclude: /node_modules/u,
        loader: "babel-loader"
      },
      {
        test: /\.html$/u,
        use: [{ loader: "html-loader", options: { minimize: true } }]
      },
      {
        test: /\.(png|jpg|gif|woff(2)?|eot|ttf|svg)$/u,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 2048,
              fallback: "file-loader",
              name: "[path][name].[ext]",
              context: "src"
            }
          }
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/u,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              implementation: require("sass")
            }
          }
        ]
      },
      {
        test: /\.less$/u,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "less-loader",
            options: {
              modifyVars: {
                "primary-color": "#1da57a"
              },
              javascriptEnabled: true
            }
          }
        ]
      }
    ]
  },
  output: {
    publicPath: "/",
    filename: "[name].js"
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebPackPlugin({
      template: resolvePath("src/base.html"),
      filename: "options.html",
      chunks: ["options"]
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css"
    }),
    new CopyWebpackPlugin([
      {
        from: resolvePath("static")
      },
      {
        from: resolvePath(
          "node_modules/webextension-polyfill/dist/browser-polyfill.min.*"
        ),
        to: "vendor/[name].[ext]"
      },
      {
        from: resolvePath("node_modules/jquery/dist/jquery.slim.min.*"),
        to: "vendor/[name].[ext]"
      }
    ]),
    new OptimizeCSSAssetsPlugin({
      cssProcessorOptions: {
        map: { inline: true },
        discardComments: { removeAll: true }
      }
    })
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
    alias: {
      "@": resolvePath("src/"),
      "@ant-design/icons/lib/dist$": resolvePath("src/antdIcon.ts")
    }
  },
  optimization: {
    minimize: false
  }
};
