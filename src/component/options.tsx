import React, { Component } from "react";
import { Form as AntdForm, Layout, message, Modal } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

import { Content, Footer, Header } from "./layout";
import { OptionsForm } from "./form";

type OwnProps = {};
type ComponentProps = OwnProps & FormComponentProps;

class Options extends Component<ComponentProps> {
  private onSave = () => {
    const { form } = this.props;
    form.validateFields((errors, values) => {
      if (errors) {
        return;
      }
      if (typeof browser !== "undefined") {
        browser.storage.local
          .set({ options: values })
          .then(() => {
            message.success("Setting saved");
          })
          .catch(error => message.error(error));
      }
    });
  };

  private onHelp = () => {
    Modal.info({
      title: "Help",
      content: (
        <div>
          <h2>Szurubooru</h2>
          <h3>Szurubooru API</h3>
          <p>Example: https://example.com/api</p>
          <p>
            You server MUST has appropriate
            &quot;Access-Control-Allow-Origin&quot; and
            &quot;Access-Control-Allow-Headers&quot; and supports OPTIONS.
          </p>
          <p>They are NOT supported on default Szurubooru.</p>
          <h3>User name</h3>
          <p>User name of Szurubooru</p>
          <h3>Password</h3>
          <p>Password of Szurubooru</p>
          <h3>Token</h3>
          <p>Token for Szurubooru login</p>
          <h3>Test Connection</h3>
          <p>Test the login to Szurubooru</p>
          <h3>Create Token</h3>
          <p>Create a token from Szurubooru. Use token to login instead.</p>
          <h2>Twitter</h2>
          <h3>Enable Template</h3>
          <p>Create a comment on Twitter post.</p>
          <h3>Comment Template</h3>
          <p>Uses Mustache template</p>
          <p>Support variables: author, authorUrl, content, url</p>
          <h2>Pixiv</h2>
          <h3>Enable Template</h3>
          <p>Create a comment on Pixiv post.</p>
          <h3>Comment Template</h3>
          <p>Uses Mustache template</p>
          <p>Support variables: author, authorUrl, url, title, content</p>
        </div>
      )
    });
  };

  public componentDidMount(): void {
    const { form } = this.props;
    if (typeof browser !== "undefined") {
      browser.storage.local
        .get("options")
        .then(values => {
          form.setFieldsValue(values.options);
        })
        .catch(error => message.error(error));
    }
  }

  public render(): JSX.Element {
    const { form } = this.props;
    return (
      <Layout className="layout">
        <Header onSave={this.onSave} onHelp={this.onHelp} />
        <Content>
          <OptionsForm form={form} />
        </Content>
        <Footer />
      </Layout>
    );
  }
}

export default AntdForm.create()(Options);
