import React, { PureComponent } from "react";
import { Icon, Layout, Menu } from "antd";

type ComponentProps = {
  onSave?: () => void;
  onHelp?: () => void;
};

export default class Header extends PureComponent<ComponentProps> {
  public render(): JSX.Element {
    const { onSave, onHelp } = this.props;
    return (
      <Layout.Header>
        <div className="logo">
          <img src="/icon/icon-48.png" />
        </div>
        <Menu mode="horizontal" selectable={false}>
          <Menu.Item onClick={onSave}>
            <Icon type="save" />
            Save
          </Menu.Item>
          <Menu.Item onClick={onHelp}>
            <Icon type="question" />
            Help
          </Menu.Item>
        </Menu>
      </Layout.Header>
    );
  }
}
