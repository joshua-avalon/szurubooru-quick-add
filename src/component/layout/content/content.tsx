import React, { PureComponent } from "react";
import { Layout } from "antd";

export default class Content extends PureComponent {
  public render(): JSX.Element {
    const { children } = this.props;
    return (
      <Layout.Content>
        <div className="inner-content">{children}</div>
      </Layout.Content>
    );
  }
}
