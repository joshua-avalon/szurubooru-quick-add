import React, { PureComponent } from "react";
import { Col, Form, Row } from "antd";
import { FormComponentProps } from "antd/lib/form";

import { TestButton, TokenButton } from "../../button";

export class ServerButtonRow extends PureComponent<FormComponentProps> {
  public render(): JSX.Element {
    const { form } = this.props;
    return (
      <Form.Item className="button-items">
        <Row gutter={8}>
          <Col span={12}>
            <TestButton form={form} />
          </Col>
          <Col span={12}>
            <TokenButton form={form} />
          </Col>
        </Row>
      </Form.Item>
    );
  }
}
