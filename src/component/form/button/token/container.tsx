import React, { MouseEvent, PureComponent } from "react";
import { connect } from "react-redux";
import { message } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

import { Authorization } from "@/api";
import { Dispatch, State } from "@/store";
import { createToken, createTokenLoadingSelector } from "@/store/createToken";

import { TokenButton } from "./button";

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mapStateToProps = (state: State) => ({
  loading: createTokenLoadingSelector(state)
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  createToken: (apiUrl: string, userName: string, auth?: Authorization) =>
    dispatch(createToken(apiUrl, userName, auth))
});
/* eslint-enable @typescript-eslint/explicit-function-return-type */

type StateProps = ReturnType<typeof mapStateToProps>;

type DispatchProps = ReturnType<typeof mapDispatchToProps>;

type OwnProps = FormComponentProps;

type ComponentProps = OwnProps & StateProps & DispatchProps;

class TokenButtonContainer extends PureComponent<ComponentProps> {
  private onValidate = (errors: any, values: any) => {
    if (errors) {
      return;
    }

    const { createToken, form } = this.props;
    const { apiUrl = "", userName = "", password } = values;
    const auth = Authorization.basic(userName, password);
    createToken(apiUrl, userName, auth)
      .then(token => {
        message.success("Create Token Success");
        form.setFieldsValue({ token, password: undefined });
      })
      .catch(() => {
        message.error("Create Token Fail");
      });
  };

  private onClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields(["apiUrl", "userName", "password"], this.onValidate);
  };

  public render(): JSX.Element {
    const { loading } = this.props;
    return <TokenButton loading={loading} onClick={this.onClick} />;
  }
}

export default connect<StateProps, DispatchProps, OwnProps, State>(
  mapStateToProps,
  mapDispatchToProps
)(TokenButtonContainer);
