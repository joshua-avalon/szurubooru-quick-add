import React, { MouseEvent, PureComponent } from "react";
import { connect } from "react-redux";
import { message } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

import { Authorization } from "@/api";
import { Dispatch, State } from "@/store";
import { test, testLoadingSelector } from "@/store/test";

import { TestButton } from "./button";

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mapStateToProps = (state: State) => ({
  loading: testLoadingSelector(state)
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  test: (apiUrl: string, auth?: Authorization) => dispatch(test(apiUrl, auth))
});
/* eslint-enable @typescript-eslint/explicit-function-return-type */

type StateProps = ReturnType<typeof mapStateToProps>;

type DispatchProps = ReturnType<typeof mapDispatchToProps>;

type OwnProps = FormComponentProps;

type ComponentProps = OwnProps & StateProps & DispatchProps;

class TestButtonContainer extends PureComponent<ComponentProps> {
  private onValidate = (errors: any, values: any) => {
    if (errors) {
      return;
    }

    const { test } = this.props;
    const { apiUrl = "", userName = "", password, token } = values;
    const auth = token
      ? Authorization.token(userName, token)
      : Authorization.basic(userName, password);
    test(apiUrl, auth)
      .then(() => {
        message.success("Connection Success");
      })
      .catch(() => {
        message.error("Connection Fail");
      });
  };

  private onClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields(
      ["apiUrl", "userName", "password", "token"],
      this.onValidate
    );
  };

  public render(): JSX.Element {
    const { loading } = this.props;
    return <TestButton loading={loading} onClick={this.onClick} />;
  }
}

export default connect<StateProps, DispatchProps, OwnProps, State>(
  mapStateToProps,
  mapDispatchToProps
)(TestButtonContainer);
