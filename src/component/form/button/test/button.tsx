import React, { MouseEvent, PureComponent } from "react";
import { Button } from "antd";

type ComponentProps = {
  loading?: boolean;
  onClick?: (e: MouseEvent<HTMLElement>) => void;
};

export class TestButton extends PureComponent<ComponentProps> {
  public render(): JSX.Element {
    const { loading, onClick } = this.props;
    return (
      <Button
        type="primary"
        htmlType="button"
        ghost
        loading={loading}
        onClick={onClick}
        icon="file-unknown">
        Test Connection
      </Button>
    );
  }
}
