import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { StringField } from "./base";
import { ComponentProps } from "./base/string";

export class TokenField extends Component<FormComponentProps> {
  private config = {
    fieldId: "token",
    type: "password",
    icon: "key",
    placeholder: "Token"
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <StringField {...fieldProps} />;
  }
}
