import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { StringField } from "./base";
import { ComponentProps } from "./base/string";

export class ServerField extends Component<FormComponentProps> {
  private config = {
    fieldId: "apiUrl",
    icon: "cloud",
    placeholder: "Szurubooru API",
    required: true,
    message: "Szurubooru API is required."
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <StringField {...fieldProps} />;
  }
}
