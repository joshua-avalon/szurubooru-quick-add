import React, { Component } from "react";
import { Switch as AntdSwitch, Form } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";

type OwnProps = {
  fieldId: string;
  label?: string;
  required?: boolean;
  message?: string;
};

export type ComponentProps = OwnProps & FormComponentProps;

export class Switch extends Component<ComponentProps> {
  public render(): JSX.Element {
    const { form, fieldId, required, message, label } = this.props;
    let layout = {};
    if (label) {
      layout = {
        labelCol: { span: 10 },
        wrapperCol: { span: 14 }
      };
    }
    const { getFieldDecorator } = form;
    return (
      <Form.Item label={label} {...layout} colon={false}>
        {getFieldDecorator(fieldId, {
          valuePropName: "checked",
          rules: [{ required, message }]
        })(<AntdSwitch />)}
      </Form.Item>
    );
  }
}
