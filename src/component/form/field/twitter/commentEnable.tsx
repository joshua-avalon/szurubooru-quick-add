import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { Switch } from "../base";
import { ComponentProps } from "../base/switch";

export class TwitterCommentEnableField extends Component<FormComponentProps> {
  private config = {
    fieldId: "twitterTemplateEnable",
    label: "Enable Template"
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <Switch {...fieldProps} />;
  }
}
