import React, { Component } from "react";
import { FormComponentProps } from "antd/lib/form/Form";

import { StringField } from "./base";
import { ComponentProps } from "./base/string";

export class PasswordField extends Component<FormComponentProps> {
  private config = {
    fieldId: "password",
    type: "password",
    icon: "lock",
    placeholder: "Password"
  };

  public render(): JSX.Element {
    const { form } = this.props;
    const fieldProps: ComponentProps = { form, ...this.config };
    return <StringField {...fieldProps} />;
  }
}
