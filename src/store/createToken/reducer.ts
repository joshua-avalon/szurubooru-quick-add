import { Actions, ActionTypes } from "./action";

export type State = { loading: boolean; success: boolean };
const initialState: State = {
  loading: false,
  success: false
};

export const reducer = (state = initialState, action: Actions): State => {
  switch (action.type) {
    case ActionTypes.CREATE_TOKEN:
      return { ...state, loading: true };
    case ActionTypes.CREATE_TOKEN_SUCCESS:
      return { ...state, loading: false, success: true };
    case ActionTypes.CREATE_TOKEN_FAIL:
      return { ...state, loading: false, success: false };
    default:
      return state;
  }
};
