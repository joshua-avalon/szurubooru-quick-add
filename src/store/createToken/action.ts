import { ActionsUnion, Dispatch } from "@/store/type";
import { State } from "@/store/state";
import { createAction } from "@/store/action";
import { HttpError } from "@/error";
import { Authorization, SzurubooruApi } from "@/api";

import { createTokenLoadingSelector } from "./select";

export enum ActionTypes {
  CREATE_TOKEN = "CREATE_TOKEN",
  CREATE_TOKEN_SUCCESS = "CREATE_TOKEN_SUCCESS",
  CREATE_TOKEN_FAIL = "CREATE_TOKEN_FAIL"
}

export const Actions = {
  createToken: () => createAction(ActionTypes.CREATE_TOKEN),
  createTokenSuccess: () => createAction(ActionTypes.CREATE_TOKEN_SUCCESS),
  createTokenFail: (status: number, message: string) =>
    createAction(ActionTypes.CREATE_TOKEN_FAIL, { status, message })
};

export type Actions = ActionsUnion<typeof Actions>;

function shouldAct(state: State): boolean {
  return !createTokenLoadingSelector(state);
}

export function createToken(
  apiUrl: string,
  userName: string,
  auth?: Authorization
): (dispatch: Dispatch, getState: () => State) => Promise<string> {
  return async (dispatch: Dispatch, getState: () => State): Promise<string> => {
    const state = getState();
    if (!shouldAct(state)) {
      return Promise.reject(new Error("Already loading"));
    }
    dispatch(Actions.createToken());
    const api = new SzurubooruApi(apiUrl, auth);
    try {
      const userToken = await api.createToken(userName);
      dispatch(Actions.createTokenSuccess());
      return userToken.token;
    } catch (error) {
      const status = error instanceof HttpError ? error.status : -1;
      dispatch(Actions.createTokenFail(status, error.message));
      return Promise.reject(error);
    }
  };
}
