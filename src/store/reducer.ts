import { combineReducers } from "redux";

import { reducer as test } from "./test";
import { reducer as createToken } from "./createToken";

export const reducer = combineReducers({ test, createToken });
