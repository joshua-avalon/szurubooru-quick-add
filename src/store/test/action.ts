import { ActionsUnion, Dispatch } from "@/store/type";
import { State } from "@/store/state";
import { createAction } from "@/store/action";
import { HttpError } from "@/error";
import { Authorization, Info, SzurubooruApi } from "@/api";

import { testLoadingSelector } from "./select";

export enum ActionTypes {
  TEST_CONNECTION = "TEST_CONNECTION",
  TEST_CONNECTION_SUCCESS = "TEST_CONNECTION_SUCCESS",
  TEST_CONNECTION_FAIL = "TEST_CONNECTION_FAIL"
}

export const Actions = {
  testConnection: () => createAction(ActionTypes.TEST_CONNECTION),
  testConnectionSuccess: () =>
    createAction(ActionTypes.TEST_CONNECTION_SUCCESS),
  testConnectionFail: (status: number, message: string) =>
    createAction(ActionTypes.TEST_CONNECTION_FAIL, { status, message })
};

export type Actions = ActionsUnion<typeof Actions>;

function shouldAct(state: State): boolean {
  return !testLoadingSelector(state);
}

export function test(
  apiUrl: string,
  auth?: Authorization
): (dispatch: Dispatch, getState: () => State) => Promise<Info> {
  return async (dispatch: Dispatch, getState: () => State): Promise<Info> => {
    const state = getState();
    if (!shouldAct(state)) {
      return Promise.reject(new Error("Already loading"));
    }
    dispatch(Actions.testConnection());
    const api = new SzurubooruApi(apiUrl, auth);
    try {
      const info = await api.info();
      dispatch(Actions.testConnectionSuccess());
      return info;
    } catch (error) {
      const status = error instanceof HttpError ? error.status : -1;
      dispatch(Actions.testConnectionFail(status, error.message));
      return Promise.reject(error);
    }
  };
}
