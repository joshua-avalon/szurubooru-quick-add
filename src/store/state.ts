import { reducer as test } from "./test";
import { reducer as createToken } from "./createToken";

export type State = {
  test: ReturnType<typeof test>;
  createToken: ReturnType<typeof createToken>;
};
