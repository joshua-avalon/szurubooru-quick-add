export interface MicroUser {
  name: string;
  avatarUrl: string;
}

export interface User extends MicroUser {
  creationTime: string;
  lastLoginTime: string;
  version: number;
  rank: UserRank;
  avatarStyle: string;
  commentCount: number;
  uploadedPostCount: number;
  favoritePostCount: number;
  likedPostCount: number;
  dislikedPostCount: number;
  email: string | null;
}

export enum UserRank {
  Administrator = "administrator",
  Anonymous = "anonymous",
  Moderator = "moderator",
  Power = "power",
  Regular = "regular"
}
