import { MicroUser } from "@/api/model/user";
import { MicroTag } from "@/api/model/tag";
import { Note } from "@/api/model/note";

export interface MicroPost {
  id: number;
  thumbnailUrl: string;
}

export interface Post {
  id: number;
  version: number;
  creationTime: string | null;
  lastEditTime: string | null;
  safety: Safety;
  source: number | null;
  type: string;
  mimeType: string;
  checksum: string;
  fileSize: number;
  canvasWidth: number;
  canvasHeight: number;
  contentUrl: string;
  thumbnailUrl: string;
  flags: string[];
  tags: MicroTag[];
  relations: MicroPost[];
  user: MicroUser;
  score: number;
  ownScore: number;
  ownFavorite: boolean;
  tagCount: number;
  favoriteCount: number;
  commentCount: number;
  noteCount: number;
  relationCount: number;
  featureCount: number;
  lastFeatureTime: string | null;
  favoritedBy: MicroUser[];
  hasCustomThumbnail: boolean;
  notes: Note[];
  comments: Comment[];
}

export enum Safety {
  Safe = "safe",
  Sketchy = "sketchy",
  Unsafe = "unsafe"
}
