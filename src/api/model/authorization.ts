function encode(value: string): string {
  return btoa(unescape(encodeURIComponent(value)));
}

export class Authorization {
  public header: string;

  public constructor(header: string) {
    this.header = header;
  }

  public static basic(user: string, password: string): Authorization {
    const encoded = encode(`${user}:${password}`);
    return new Authorization(`Basic ${encoded}`);
  }

  public static token(user: string, token: string): Authorization {
    const encoded = encode(`${user}:${token}`);
    return new Authorization(`Token ${encoded}`);
  }
}
