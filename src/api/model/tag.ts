export interface MicroTag {
  names: string[];
  category: string;
  usages: number;
}
