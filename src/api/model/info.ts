import { User, UserRank } from "./user";
import { Post } from "./post";

export interface Info {
  postCount: number;
  diskUsage: number;
  serverTime: string;
  config: Config;
  featuredPost: Post;
  featuringUser: User;
  featuringTime: string;
}

export interface Config {
  name: string;
  userNameRegex: string;
  passwordRegex: string;
  tagNameRegex: string;
  tagCategoryNameRegex: string;
  defaultUserRank: UserRank;
  enableSafety: boolean;
  contactEmail: string | null;
  canSendMails: boolean;
  privileges: { [key: string]: UserRank };
}
