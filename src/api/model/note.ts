export interface Note {
  polygon: number[][];
  text: string;
}
