import { MicroUser } from "@/api/model/user";

export interface Comment {
  id: number;
  user: MicroUser;
  postId: number;
  version: number;
  text: string;
  creationTime: string | null;
  lastEditTime: string | null;
  score: number;
  ownScore: number;
}
