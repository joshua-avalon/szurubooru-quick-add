import { MicroUser } from "./user";

export interface UserToken {
  user: MicroUser;
  token: string;
  note: string | null;
  enabled: boolean;
  expirationTime: string | null;
  creationTime: string | null;
  lastEditTime: string | null;
  lastUsageTime: string | null;
  version: number;
}
