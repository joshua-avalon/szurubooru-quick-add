export default class ApiRequest {
  private readonly config: RequestInit;

  public constructor(config: RequestInit) {
    this.config = config;
  }

  public fetch(url: string, extra?: RequestInit): Promise<Response> {
    return fetch(url, { ...this.config, ...extra });
  }
}
