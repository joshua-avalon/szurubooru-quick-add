import { Authorization } from "@/api";

import ApiRequest from "./request";

export default class Factory {
  private readonly config: RequestInit;

  public constructor(auth?: Authorization) {
    const headers: { [key: string]: string } = {
      Accept: "application/json; charset=utf-8",
      "Content-Type": "application/json"
    };
    if (auth) {
      headers.Authorization = auth.header;
    }
    this.config = { headers };
  }

  public create(method?: string, json?: { [key: string]: any }): ApiRequest {
    const config = { ...this.config };
    if (json) {
      config.body = JSON.stringify(json);
    }
    if (method) {
      config.method = method;
    }
    return new ApiRequest(config);
  }
}
