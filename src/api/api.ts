import urlJoin from "url-join";

import { HttpError } from "@/error";

import { Factory } from "./request";
import { Authorization, Comment, Info, Post, Safety, UserToken } from "./model";

export default class Api {
  private readonly factory: Factory;
  private readonly apiUrl: string;

  public constructor(apiUrl: string, auth?: Authorization) {
    this.apiUrl = apiUrl;
    this.factory = new Factory(auth);
  }

  public info(): Promise<Info> {
    return this.get("info");
  }

  public createToken(userName: string): Promise<UserToken> {
    return this.post(`user-token/${userName}`, {
      enabled: true,
      note: "Szurubooru Quick Add"
    });
  }

  public createPost(
    contentUrl: string,
    relations: number[] = [],
    tags: string[] = [],
    safety: Safety = Safety.Safe
  ): Promise<Post> {
    return this.post("posts", { contentUrl, relations, tags, safety });
  }

  public getPost(postId: number): Promise<Post> {
    return this.get(`post/${postId}`);
  }

  public updatePost(
    postId: number,
    version: number,
    source?: string,
    relations?: number[]
  ): Promise<Post> {
    return this.put(`post/${postId}`, { postId, version, relations, source });
  }

  public createComment(postId: number, text: string): Promise<Comment> {
    return this.post("comments", { postId, text });
  }

  private url(api: string): string {
    return urlJoin(this.apiUrl, api);
  }

  private get(api: string): Promise<any> {
    const url = this.url(api);
    return this.handle(this.factory.create().fetch(url));
  }

  private post(api: string, json: { [key: string]: any }): Promise<any> {
    const url = this.url(api);
    return this.handle(this.factory.create("POST", json).fetch(url));
  }

  private put(api: string, json: { [key: string]: any }): Promise<any> {
    const url = this.url(api);
    return this.handle(this.factory.create("PUT", json).fetch(url));
  }

  private handle(promise: Promise<Response>): Promise<any> {
    return promise
      .then(response =>
        Promise.all([Promise.resolve(response), response.json()])
      )
      .then(values => {
        const response: Response = values[0];
        const json = values[1];
        if (!response.ok) {
          if (json && json.description) {
            throw new HttpError(response.status, json.description);
          }
          throw new HttpError(response.status, response.statusText);
        }
        return json;
      });
  }
}
