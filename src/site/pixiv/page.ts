import { EventType } from "@/model";

declare var globalInitData: any;

window.postMessage(
  {
    type: EventType.Pixiv,
    payload: globalInitData
  },
  "*"
);
