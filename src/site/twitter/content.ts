import { Tweet } from "@/model";

function upload(): Tweet {
  const path = window.location.pathname;
  const tweet = $(`.tweet[data-permalink-path="${path}"]`);
  tweet.find(".AdaptiveMedia-photoContainer");
  const containers = tweet.find(".AdaptiveMedia-photoContainer");
  const imagesSet = new Set<string>();
  containers.each(function() {
    imagesSet.add($(this).data("image-url"));
  });
  const images: string[] = [...imagesSet];

  const contentElement = tweet
    .find(".tweet-text")
    .first()
    .clone();
  contentElement.find(".u-hidden").remove();
  const content = contentElement.text() || "";
  const url = window.location.href;
  const author =
    tweet
      .find(".fullname")
      .first()
      .text() || "";
  const pattern = /\/(.*)\/status\/.*/u;
  const match = path.match(pattern);
  const authorUrl = match
    ? `https://twitter.com/${match[1]}`
    : "https://twitter.com";
  return { images, content, url, author, authorUrl, type: "twitter" };
}

browser.runtime.onMessage.addListener((request, _, sendResponse) => {
  if (request.action !== "upload") {
    return;
  }
  sendResponse(upload());
});
