import Mustache from "mustache";

import { Pixiv, Tweet } from "@/model";
import { Authorization, Post, SzurubooruApi } from "@/api";

enum MenuItem {
  Upload = "upload"
}

type UploadResponse = Tweet | Pixiv;

async function createApi(): Promise<SzurubooruApi> {
  const values = await browser.storage.local.get("options");
  const { apiUrl = "", userName = "", password = "", token = "" } =
    values.options || {};
  const auth = token
    ? Authorization.token(userName, token)
    : Authorization.basic(userName, password);
  return new SzurubooruApi(apiUrl, auth);
}

async function showSuccessNotification(): Promise<void> {
  const id = await browser.notifications.create({
    type: "basic",
    iconUrl: browser.extension.getURL("icon/icon-48.png"),
    title: "Szurubooru Quick Add",
    message: "Upload success"
  });
  if (id) {
    setTimeout(() => browser.notifications.clear(id), 3000);
  }
}

async function showFailureNotification(message: string): Promise<void> {
  const id = await browser.notifications.create({
    type: "basic",
    iconUrl: browser.extension.getURL("icon/icon-48.png"),
    title: "Szurubooru Quick Add",
    message
  });
  if (id) {
    setTimeout(() => browser.notifications.clear(id), 3000);
  }
}

async function uploadImage(
  images: string[],
  url: string,
  comment?: string
): Promise<Post[]> {
  const posts: Post[] = [];
  const api = await createApi();
  try {
    for (const image of images) {
      const post = await api.createPost(image, posts.map(post => post.id));
      posts.push(post);
    }

    await Promise.all(
      posts.map(({ id, version }) => api.updatePost(id, version, url))
    );
  } catch (error) {
    showFailureNotification(error.message || "Unknown error");
    console.error({ error });
    return [];
  }

  if (!comment || posts.length <= 0) {
    showSuccessNotification();
    return posts;
  }
  await api.createComment(posts[0].id, comment);
  showSuccessNotification();
  return posts;
}

async function uploadTwitter(response: Tweet): Promise<Post[]> {
  const { options } = await browser.storage.local.get("options");
  const { twitterTemplateEnable, twitterComment = "" } = options;
  const comment = twitterTemplateEnable
    ? Mustache.render(twitterComment, response)
    : undefined;
  return uploadImage(response.images, response.url, comment);
}

async function uploadPixiv(response: Pixiv): Promise<Post[]> {
  const { options } = await browser.storage.local.get("options");
  const { pixivTemplateEnable, pixivComment = "" } = options;
  const comment = pixivTemplateEnable
    ? Mustache.render(pixivComment, response)
    : undefined;
  return uploadImage(response.images, response.url, comment);
}

function upload(response: UploadResponse): void {
  if (!response) {
    return;
  }
  switch (response.type) {
    case "twitter":
      uploadTwitter(response);
      break;
    case "pixiv":
      uploadPixiv(response);
      break;
  }
}

browser.contextMenus.create({
  id: MenuItem.Upload,
  title: browser.i18n.getMessage("menuItemUpload"),
  contexts: ["all"],
  documentUrlPatterns: [
    "*://twitter.com/*/status/*",
    "*://www.pixiv.net/member_illust.php*"
  ]
});

browser.contextMenus.onClicked.addListener((info, tab) => {
  if (!tab) {
    return;
  }
  switch (info.menuItemId) {
    case MenuItem.Upload:
      browser.tabs
        .sendMessage(tab.id as number, { action: "upload" })
        .then(upload);
      break;
  }
});

browser.runtime.onMessage.addListener((message: UploadResponse) =>
  upload(message)
);
