// https://github.com/ant-design/ant-design/issues/12011

export {
  default as SaveOutline
} from "@ant-design/icons/lib/outline/SaveOutline";

export {
  default as QuestionOutline
} from "@ant-design/icons/lib/outline/QuestionOutline";

export {
  default as UserOutline
} from "@ant-design/icons/lib/outline/UserOutline";

export {
  default as LockOutline
} from "@ant-design/icons/lib/outline/LockOutline";

export {
  default as KeyOutline
} from "@ant-design/icons/lib/outline/KeyOutline";

export {
  default as CloudOutline
} from "@ant-design/icons/lib/outline/CloudOutline";

export {
  default as FileUnknownOutline
} from "@ant-design/icons/lib/outline/FileUnknownOutline";

export {
  default as InfoCircleOutline
} from "@ant-design/icons/lib/outline/InfoCircleOutline";
