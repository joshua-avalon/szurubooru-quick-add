export interface Pixiv {
  type: "pixiv";
  author: string;
  authorUrl: string;
  url: string;
  title: string;
  content: string;
  images: string[];
}

export interface PixivData {
  token: string;
  services: Services;
  isMobile: boolean;
  oneSignalAppId: string;
  publicPath: string;
  commonResourcePath: string;
  development: boolean;
  userData: UserData;
  premium: Premium;
  preload: Preload;
  mute: any[];
}

interface Preload {
  timestamp: string;
  illust: { [key: string]: Illust };
  user: { [key: string]: User };
}

interface Illust {
  illustId: string;
  illustTitle: string;
  illustComment: string;
  id: string;
  title: string;
  description: string;
  illustType: number;
  createDate: string;
  uploadDate: string;
  restrict: number;
  xRestrict: number;
  urls: Urls;
  tags: Tags;
  storableTags: string[];
  userId: string;
  userName: string;
  userAccount: string;
  userIllusts: { [key: string]: UserIllust | null };
  likeData: boolean;
  width: number;
  height: number;
  pageCount: number;
  bookmarkCount: number;
  likeCount: number;
  commentCount: number;
  responseCount: number;
  viewCount: number;
  isHowto: boolean;
  isOriginal: boolean;
  imageResponseOutData: any[];
  imageResponseData: any[];
  imageResponseCount: number;
  pollData: null;
  seriesNavData: null;
  descriptionBoothId: null;
  comicPromotion: null;
  contestBanners: any[];
  factoryGoods: FactoryGoods;
  isBookmarkable: boolean;
  bookmarkData: null;
  zoneConfig: ZoneConfig;
}

interface FactoryGoods {
  integratable: boolean;
  integrated: boolean;
}

interface Tags {
  authorId: string;
  isLocked: boolean;
  tags: Tag[];
  writable: boolean;
}

interface Tag {
  tag: string;
  locked: boolean;
  deletable: boolean;
  userId?: string;
  romaji: null | string;
  translation?: Translation;
  userName?: string;
}

interface Translation {
  en: string;
}

interface Urls {
  mini: string;
  thumb: string;
  small: string;
  regular: string;
  original: string;
}

interface UserIllust {
  illustId: string;
  illustTitle: string;
  id: string;
  title: string;
  illustType: number;
  xRestrict: number;
  url: string;
  tags: string[];
  userId: string;
  width: number;
  height: number;
  pageCount: number;
  isBookmarkable: boolean;
  bookmarkData: null;
}

interface ZoneConfig {
  responsive: Config;
  "300x250": Config;
  "500x500": Config;
  header: Config;
  footer: Config;
  expandedFooter: Config;
  logo: Config;
}

interface Config {
  url: string;
}

interface User {
  userId: string;
  name: string;
  image: string;
  imageBig: string;
  premium: boolean;
  isFollowed: boolean;
  background: null;
  partial: number;
}

interface Premium {
  popularSearch: boolean;
  adsHide: boolean;
  novelCoverReupload: boolean;
}

interface Services {
  booth: string;
  factory: string;
  sketch: string;
}

interface UserData {
  id: string;
  name: string;
  profileImg: string;
  premium: boolean;
  xRestrict: number;
  adult: boolean;
}
