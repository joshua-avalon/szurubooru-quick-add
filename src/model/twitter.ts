export interface Tweet {
  type: "twitter";
  author: string;
  authorUrl: string;
  content: string;
  url: string;
  images: string[];
}
